# About the Terraform Language

The main purpose of the Terraform language is declaring resources, which represent infrastructure objects. All other language features exist only to make the definition of resources more flexible and convenient.

A Terraform configuration is a complete document in the Terraform language that tells Terraform how to manage a given collection of infrastructure. A configuration can consist of multiple files and directories.

# HCL - What's is It?

Terraform language, which is a rich language designed to be relatively easy for humans to read and write. The constructs in the Terraform language can also be expressed in JSON syntax, which is harder for humans to read and edit but easier to generate and parse programmatically.

This low-level syntax of the Terraform language is defined in terms of a syntax called HCL, which is also used by configuration languages in other applications, and in particular other HashiCorp products. It is not necessary to know all of the details of HCL syntax in order to use Terraform

The Terraform language syntax is built around two key syntax constructs: arguments and blocks.

## Arguments

An argument assigns a value to a particular name:

```terraform
image_id = "abc123"
```

## Blocks

A block is a container for other content:

```terraform
resource "aws_instance" "example" {
  ami = "abc123"

  network_interface {
    # ...
  }
}
```

# State

Terraform must store state about your managed infrastructure and configuration. This state is used by Terraform to map real world resources to your configuration, keep track of metadata, and to improve performance for large infrastructures.

This state is stored by default in a local file named "terraform.tfstate", but it can also be stored remotely, which works better in a team environment.

Terraform uses this local state to create plans and make changes to your infrastructure. Prior to any operation, Terraform does a refresh to update the state with the real infrastructure.

# Terraform - Basic Commands

Mainly when you are working with terraform there are 4 commands that you must keep in mind:

- terraform init
- terraform plan
- terraform apply
- terraform destroy

## terraform init

This command is responsible for installing the plugin that will comunicate with your cloud provider, and manage the place where your statefile will be stored. It can be stored at your cloud provider (S3 in AWS, for example), or locally.

## terraform plan

This command is responsible for reading all the .tf files in your folder, check and present the results of a "dry run" execution (Thanks K8S!) or your Terraform files. Its very important to execute this command passing the parameter "-out outputfile", because then you can guarantee that the execution of the apply command will follow strictly the results of the planning done.

## terraform apply

This command is responsible for executing your plan done by the "plan" command. It's important to inform to the "apply" command your plan file generated in the step before.

## terraform destroy

This command will read your HCL file and destroy your infra. As simple as it is. :)

# Resources

Resources are the most important element in the Terraform language. Each resource block describes one or more infrastructure objects, such as virtual networks, compute instances, or higher-level components such as DNS records.

## Resource Syntax

Resource declarations can include a number of advanced features, but only a small subset are required for initial use. More advanced syntax features, such as single resource declarations that produce multiple similar remote objects, are described later in this page.

```terraform
resource "aws_instance" "web" {
  ami           = "ami-a1b2c3d4"
  instance_type = "t2.micro"
}
```

# Providers

Terraform relies on plugins called "providers" to interact with cloud providers, SaaS providers, and other APIs.

Terraform configurations must declare which providers they require so that Terraform can install and use them. Additionally, some providers require configuration (like endpoint URLs or cloud regions) before they can be used.

```terraform
provider "aws" {
  region  = "us-east-2"
  version = "~> 3.0"
}
```

# Data Sources

Data sources allow Terraform use information defined outside of Terraform, defined by another separate Terraform configuration, or modified by functions.

```terraform
data "aws_ami" "example" {
  most_recent = true

  owners = ["self"]
  tags = {
    Name   = "app-server"
    Tested = "true"
  }
}
```

# Variables and Outputs

The Terraform language includes a few kinds of blocks for requesting or publishing named values.

- Input Variables serve as parameters for a Terraform module, so users can customize behavior without editing the source.
- Output Values are like return values for a Terraform module.

## Input Variables

Each input variable accepted by a module must be declared using a variable block:

```terraform
variable "image_id" {
  default     = "ami-0117d177e96a8481c" # us-east-2
  type        = string
  sensitive   = true
  description = "The ID of the machine (AMI) to user for server"

  validation {
    condition     = length(var.image_id) > 4 && substr(var.image_id, 0, 4) == "ami-"
    error_message = "Please inform an AMI value that starts with \"ami-\"."
  }
}
```

The name of a variable can be any valid identifier except the following: source, version, providers, count, for_each, lifecycle, depends_on, locals. 

To access a variable the notation "var.name" must be followed:

```terraform
resource "aws_instance" "web" {
  ami           = var.image_id
  instance_type = "t2.micro"

  tags = {
    "Name" = "Hello World East"
  }
}
```

## Variables on the Command Line
To specify individual variables on the command line, use the -var option when running the terraform plan and terraform apply commands:

```bash
terraform apply -var="image_id=ami-abc123"
```

## Output

Output values are like the return values of a Terraform module, and have several uses:

- A child module can use outputs to expose a subset of its resource attributes to a parent module.
- A root module can use outputs to print certain values in the CLI output after running terraform apply.
- When using remote state, root module outputs can be accessed by other configurations via a terraform_remote_state data source.

Each output value exported by a module must be declared using an output block:

```terraform
output "instance_ip_addr" {
  value = aws_instance.server.private_ip
}
```