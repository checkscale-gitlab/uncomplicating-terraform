# dynamic Blocks

Within top-level block constructs like resources, expressions can usually be used only when assigning a value to an argument using the name = expression form. This covers many uses, but some resource types include repeatable nested blocks in their arguments, which typically represent separate objects that are related to (or embedded within) the containing object:

```terraform
resource "aws_elastic_beanstalk_environment" "tfenvtest" {
  name = "tf-test-name" # can use expressions here

  setting {
    # but the "setting" block is always a literal block
  }
}
```

You can dynamically construct repeatable nested blocks like setting using a special dynamic block type, which is supported inside resource, data, provider, and provisioner blocks:

```terraform
resource "aws_elastic_beanstalk_environment" "tfenvtest" {
  name                = "tf-test-name"
  application         = "${aws_elastic_beanstalk_application.tftest.name}"
  solution_stack_name = "64bit Amazon Linux 2018.03 v2.11.4 running Go 1.12.6"

  dynamic "setting" {
    for_each = var.settings
    content {
      namespace = setting.value["namespace"]
      name = setting.value["name"]
      value = setting.value["value"]
    }
  }
}
```

A dynamic block acts much like a for expression, but produces nested blocks instead of a complex typed value. It iterates over a given complex value, and generates a nested block for each element of that complex value.

- The label of the dynamic block ("setting" in the example above) specifies what kind of nested block to generate.
- The for_each argument provides the complex value to iterate over.
- The iterator argument (optional) sets the name of a temporary variable that represents the current element of the complex value. If omitted, the name of the variable defaults to the label of the dynamic block ("setting" in the example above).
- The labels argument (optional) is a list of strings that specifies the block labels, in order, to use for each generated block. You can use the temporary iterator variable in this value.
- The nested content block defines the body of each generated block. You can use the temporary iterator variable inside this block.
- Since the for_each argument accepts any collection or structural value, you can use a for expression or splat expression to transform an existing collection.

The iterator object (setting in the example above) has two attributes:

- key is the map key or list element index for the current element. If the for_each expression produces a set value then key is identical to value and should not be used.
- value is the value of the current element.

A dynamic block can only generate arguments that belong to the resource type, data source, provider or provisioner being configured. It is not possible to generate meta-argument blocks such as lifecycle and provisioner blocks, since Terraform must process these before it is safe to evaluate expressions.

The for_each value must be a collection with one element per desired nested block. If you need to declare resource instances based on a nested data structure or combinations of elements from multiple data structures you can use Terraform expressions and functions to derive a suitable value. For some common examples of such situations, see the flatten and setproduct functions.

# String Templates

Within quoted and heredoc string expressions, the sequences ${ and %{ begin template sequences. Templates let you directly embed expressions into a string literal, to dynamically construct strings from other values.

## Interpolation

A ${ ... } sequence is an interpolation, which evaluates the expression given between the markers, converts the result to a string if necessary, and then inserts it into the final string:

```terraform
"Hello, ${var.name}!"
```

In the above example, the named object var.name is accessed and its value inserted into the string, producing a result like "Hello, Juan!".

## Directives

A %{ ... } sequence is a directive, which allows for conditional results and iteration over collections, similar to conditional and for expressions.

The following directives are supported:

- The %{if <BOOL>}/%{else}/%{endif} directive chooses between two templates based on the value of a bool expression:

```terraform
"Hello, %{ if var.name != "" }${var.name}%{ else }unnamed%{ endif }!"
```

The else portion may be omitted, in which case the result is an empty string if the condition expression 
returns false.

- The %{for <NAME> in <COLLECTION>} / %{endfor} directive iterates over the elements of a given collection or structural value and evaluates a given template once for each element, concatenating the results together:

```terraform
%{ for ip in aws_instance.example.*.private_ip }
server ${ip}
%{ endfor }
```

The name given immediately after the for keyword is used as a temporary variable name which can then be referenced from the nested template.